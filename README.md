## About

This is a pet project aimed to get familiar with basic CI/CD workflow and AWS.
When setup is completed, every commit to the default branch will trigger a GitLab CI/CD job that runs tests. If you're ready to deliver the application, you should create a new release tag which will result in automatic deployment 

## Tools used
* Spring Boot
* Gradle
* Docker
* Kubernetes
* Helm
* AWS EKS
* Gitlab CI/CD & registry

## Setup
1. Create AWS IAM user with EKS-related privileges granted
2. Save the provided key & secret pair on your local machine (`~/.aws/credentials`) and Gitlab CI/CD variables (`$AWS_ACCESS_KEY_ID` and `$AWS_SECRET_ACCESS_KEY` correspondingly)
2. Create a Kubernetes cluster on AWS, for example
```shell
eksctl create cluster --name springboot --region eu-central-1 --nodes=4 --node-type=t2.small
```
Note that you [must](https://newbedev.com/aws-eks-0-1-nodes-are-available-1-insufficient-pods) have at least `t2.small` configuration otherwise you won't be able to run all required pods
4. Create an Ingress controller, for example
```shell
kubectl apply -f https://projectcontour.io/quickstart/contour.yaml
```
5. Set `$AWS_KUBECTL_CONFIG` variable with `~/.kube/config` content
5. Generate a read-only [deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/) and create Kubernetes secret with "registry-credentials" name following [the instruction](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/). This will allow the cluster to access private Gitlab registry
6. Secret from above must be saved to `$REGISTRY_CREDENTIALS` variable as well
7. Update [Helm values](https://gitlab.com/ojdgaf/spring-boot-with-aws-eks/-/blob/main/.helm/values.yaml)
8. Don't forget to delete the cluster after testing
```shell
eksctl delete cluster --name springboot
```
