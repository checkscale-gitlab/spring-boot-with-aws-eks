package com.ojdgaf.springboot;

import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log
@RestController
@RequestMapping("v1")
public class ControllerV1 {

    @GetMapping("user")
    public String getUser() {
        log.info("GET /v1/user");
        return "User v1";
    }

    @GetMapping("data")
    public String getData() {
        log.info("GET /v1/data");
        return "Data v1";
    }

    @GetMapping("test")
    public String getTest() {
        log.info("GET /v1/test");
        return "Test v1";
    }
}
